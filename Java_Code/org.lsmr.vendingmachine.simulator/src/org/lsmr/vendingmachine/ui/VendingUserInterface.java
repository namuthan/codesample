package org.lsmr.vendingmachine.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import java.util.Timer;
import java.util.TimerTask;

import org.lsmr.vendingmachine.core.*;
import org.lsmr.vendingmachine.simulator.Coin;
import org.lsmr.vendingmachine.simulator.PopCan;

public class VendingUserInterface extends JFrame {

	private JButton pop1;
	private JButton pop2;
	private JButton pop3;
	private JButton pop4;
	private JButton pop5;
	private JButton pop6;
	private JButton pop7;
	private JButton pop8;
	private JButton pop9;
	private JButton pop10;
	private JButton coinReturn;
	private JButton nickle;
	private JButton dime;
	private JButton quarter;
	private JButton loonie;
	private JButton toonie;
	private JPanel display;
	private JPanel buttons;
	private JLabel output;
	private JLabel outOfOrder;
	private JLabel exactChange;
	private double funds = 0;
	private VendingCore core;
	private Timer delayTimer;
	private final int SHORT_DELAY = 4000;
	private final int LONG_DELAY = 5000;
	private boolean inUse = false;
	private boolean receptacleFull = false;
	private boolean disabled = false;
	private Object[] items;
	private Coin coin;
	private PopCan can;

	public VendingUserInterface() {
		setResizable(false);
		init();
	}

	public Dimension getPreferredSize() {
		return new Dimension(500, 300);
	}

	private void init() {
		delayTimer = new Timer();
		core = new VendingCore();
		core.loadPop();
		setLayout(new GridLayout(2, 1));
		display = new JPanel();
		display.setLayout(new GridLayout(3, 3));
		output = new JLabel("0.00");
		outOfOrder = new JLabel("Out of order light off");
		exactChange = new JLabel("Exact change light on");
		core.getHardwareSimulator().getExactChangeLight().activate();
		display.add(new JLabel(""));
		display.add(output);
		display.add(new JLabel(""));
		display.add(outOfOrder);
		display.add(new JLabel(""));
		display.add(exactChange);
		buttons = new JPanel();
		buttons.setLayout(new GridLayout(4, 5));

		/**
		 * General outline for pop buttons: Each button will call buyPop(index)
		 * where index is the index number pointing to the correct pop rack.
		 * They will display the price information for 4 seconds if the user
		 * does not have enough money, which is done through a timer. I have
		 * made it so you have to wait the full 4 seconds before another pop
		 * selection can display its price.
		 */

		pop1 = new JButton(core.getProductKind(0).getName());
		pop1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				popButton(0);

			}

		});

		pop2 = new JButton(core.getProductKind(1).getName());
		pop2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				popButton(1);
			}

		});

		pop3 = new JButton(core.getProductKind(2).getName());
		pop3.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				popButton(2);
			}

		});

		pop4 = new JButton(core.getProductKind(3).getName());
		pop4.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				popButton(3);
			}

		});

		pop5 = new JButton(core.getProductKind(4).getName());
		pop5.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				popButton(4);
			}

		});

		pop6 = new JButton(core.getProductKind(5).getName());
		pop6.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				popButton(5);
			}

		});

		pop7 = new JButton(core.getProductKind(6).getName());
		pop7.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				popButton(6);
			}

		});

		pop8 = new JButton(core.getProductKind(7).getName());
		pop8.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				popButton(7);
			}

		});

		pop9 = new JButton(core.getProductKind(8).getName());
		pop9.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				popButton(8);
			}

		});

		pop10 = new JButton(core.getProductKind(9).getName());
		pop10.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				popButton(9);
			}

		});

		/**
		 * The coin return button calls core.returnCoins, and prints the value
		 * of each coin it receives from the machine.
		 */

		coinReturn = new JButton("Return");
		coinReturn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Object[] coinRet = core.returnCoins();
				funds = 0.00;
				output.setText(String.format("%.2f", funds));
				for (int i = 0; i < coinRet.length; i++) {
					coin = (Coin) coinRet[i];
					System.out.println("You got " + coin.getValue()
							+ " cents back");
				}
				
				if (receptacleFull) {
					core.getHardwareSimulator().getOutOfOrderLight().deactivate();
					outOfOrder.setText("Out of order light off");
					receptacleFull = false;
				}

			}

		});

		/**
		 * General outline for coin buttons: Each button is attached to
		 * core.insertCoin(value), where value is the value of each coin. After
		 * inserting a coin, the value of funds is modified to reflect the fund
		 * change, and this is then displayed.
		 */

		nickle = new JButton("$0.05");
		nickle.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (!core.getHardwareSimulator().getStorageBin().hasSpace()) {
					core.getHardwareSimulator().enableSafety();
					core.getHardwareSimulator().getOutOfOrderLight().activate();
					outOfOrder.setText("Out of order light on");
					output.setText("Machine is out of order");
					disabled = true;
				}
				core.insertCoin(5);
				if ((items = core.getHardwareSimulator().getDeliveryChute().removeItems()) != null
						&& items.length != 0) {
					for (int i = 0; i < items.length; i++) {
						coin = (Coin) items[i];
						System.out.println(coin.getValue() + " cents returned");
						if (!disabled) {
							core.getHardwareSimulator().getOutOfOrderLight().activate();
							outOfOrder.setText("Out of order light on");
							output.setText("Coin receptacle is full");
							receptacleFull = true;
						}
					}
				} 
				
				else {
					funds += 0.05;
					output.setText(String.format("%.2f", funds));
				}

			}

		});

		dime = new JButton("$0.10");
		dime.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (!core.getHardwareSimulator().getStorageBin().hasSpace()) {
					core.getHardwareSimulator().enableSafety();
					core.getHardwareSimulator().getOutOfOrderLight().activate();
					outOfOrder.setText("Out of order light on");
					output.setText("Machine is out of order");
					disabled = true;
				}
				core.insertCoin(10);
				if ((items = core.getHardwareSimulator().getDeliveryChute().removeItems()) != null
						&& items.length != 0) {
					for (int i = 0; i < items.length; i++) {
						coin = (Coin) items[i];
						System.out.println(coin.getValue() + " cents returned");
						if (!disabled) {
							core.getHardwareSimulator().getOutOfOrderLight().activate();
							outOfOrder.setText("Out of order light on");
							output.setText("Coin receptacle is full");
							receptacleFull = true;
						}
					}
				} else {
					funds += 0.10;
					output.setText(String.format("%.2f", funds));
				}

			}

		});

		quarter = new JButton("$0.25");
		quarter.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (!core.getHardwareSimulator().getStorageBin().hasSpace()) {
					core.getHardwareSimulator().enableSafety();
					core.getHardwareSimulator().getOutOfOrderLight().activate();
					outOfOrder.setText("Out of order light on");
					output.setText("Machine is out of order");
					disabled = true;
				}
				core.insertCoin(25);
				if ((items = core.getHardwareSimulator().getDeliveryChute().removeItems()) != null
						&& items.length != 0) {
					for (int i = 0; i < items.length; i++) {
						coin = (Coin) items[i];
						System.out.println(coin.getValue() + " cents returned");
						if (!disabled) {
							core.getHardwareSimulator().getOutOfOrderLight().activate();
							outOfOrder.setText("Out of order light on");
							output.setText("Coin receptacle is full");
							receptacleFull = true;
						}
					}
				} else {
					funds += 0.25;
					output.setText(String.format("%.2f", funds));
				}

			}

		});

		loonie = new JButton("$1.00");
		loonie.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (!core.getHardwareSimulator().getStorageBin().hasSpace()) {
					core.getHardwareSimulator().enableSafety();
					core.getHardwareSimulator().getOutOfOrderLight().activate();
					outOfOrder.setText("Out of order light on");
					output.setText("Machine is out of order");
					disabled = true;
				}
				core.insertCoin(100);
				if ((items = core.getHardwareSimulator().getDeliveryChute().removeItems()) != null
						&& items.length != 0) {
					for (int i = 0; i < items.length; i++) {
						coin = (Coin) items[i];
						System.out.println(coin.getValue() + " cents returned");
						if (!disabled) {
							core.getHardwareSimulator().getOutOfOrderLight().activate();
							outOfOrder.setText("Out of order light on");
							output.setText("Coin receptacle is full");
							receptacleFull = true;
						}
					}
				} else {
					funds += 1.00;
					output.setText(String.format("%.2f", funds));
				}

			}

		});

		toonie = new JButton("$2.00");
		toonie.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (!core.getHardwareSimulator().getStorageBin().hasSpace()) {
					core.getHardwareSimulator().enableSafety();
					outOfOrder.setText("Out of order light on");
					output.setText("Machine is out of order");
					disabled = true;
				}
				core.insertCoin(200);
				if ((items = core.getHardwareSimulator().getDeliveryChute().removeItems()) != null
						&& items.length != 0) {
					for (int i = 0; i < items.length; i++) {
						coin = (Coin) items[i];
						System.out.println(coin.getValue() + " cents returned");
						if (!disabled) {
							core.getHardwareSimulator().getOutOfOrderLight().activate();
							outOfOrder.setText("Out of order light on");
							output.setText("Coin receptacle is full");
							receptacleFull = true;
						}
					}
				} else {
					funds += 2.00;
					output.setText(String.format("%.2f", funds));
				}
			}

		});

		buttons.add(nickle);
		buttons.add(dime);
		buttons.add(quarter);
		buttons.add(loonie);
		buttons.add(toonie);
		buttons.add(pop1);
		buttons.add(pop2);
		buttons.add(pop3);
		buttons.add(pop4);
		buttons.add(pop5);
		buttons.add(pop6);
		buttons.add(pop7);
		buttons.add(pop8);
		buttons.add(pop9);
		buttons.add(pop10);
		buttons.add(new JLabel(""));
		buttons.add(new JLabel(""));
		buttons.add(coinReturn);
		buttons.add(new JLabel(""));
		buttons.add(new JLabel(""));

		add(display, BorderLayout.NORTH);
		add(buttons, BorderLayout.CENTER);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	private void popButton(int index) {
		if (core.getProductKind(index).getQuantityAvailable() > 0) {
			if ((items = core.buyPop(index)) == null) {
				if (!inUse) {
					inUse = true;
					output.setText(core.getProductKind(index).getName()
							+ " costs "
							+ core.getProductKind(index).getStandardCost()
									.getQuantity());
					delayTimer.schedule(new TimerTask() {

						@Override
						public void run() {
							output.setText(String.format("%.2f", funds));
							inUse = false;

						}

					}, SHORT_DELAY);
				}
			}

			else {
				funds = 0.00;
				if ((can = (PopCan) items[0]) != null)
					System.out.println("You got "		
							+ core.getProductKind(index).getName());
				for (int i = 1; i < items.length; i++) {
					coin = (Coin) items[i];
					System.out.println("You got " + coin.getValue()
							+ " cents back");
				}
				output.setText(String.format("%.2f", funds));
				if (receptacleFull) {
					receptacleFull = false;
					core.getHardwareSimulator().getOutOfOrderLight().deactivate();
					outOfOrder.setText("Out of order light off");
				}
			}

		} else {
			if (!inUse) {
				inUse = true;
				output.setText(core.getProductKind(index).getName()
						+ " is out of stock");
				delayTimer.schedule(new TimerTask() {

					@Override
					public void run() {
						output.setText(String.format("%.2f", funds));
						inUse = false;
					}

				}, LONG_DELAY);
			}
		}
		if (core.checkMinChange()) {
			exactChange.setText("Exact change light off");
			core.getHardwareSimulator().getExactChangeLight().deactivate();
		} else {
			exactChange.setText("Exact change light on");
			core.getHardwareSimulator().getExactChangeLight().activate();
		}

	}

}
