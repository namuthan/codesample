package org.lsmr.vendingmachine.ui;

import javax.swing.JFrame;

public class Main {
	public static void main (String[] args) {
		JFrame frame = new VendingUserInterface();
		frame.pack();
		frame.setVisible(true);
	}
}
