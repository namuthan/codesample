package org.lsmr.vendingmachine.core;

import java.math.BigDecimal;
import java.util.Locale;

import org.lsmr.vendingmachine.funds.CoinsAvailable;
import org.lsmr.vendingmachine.funds.Currency;
import org.lsmr.vendingmachine.product.ProductKind;
import org.lsmr.vendingmachine.product.ProductKindListener;
import org.lsmr.vendingmachine.simulator.*;

public class VendingCore {

	private String[] names = { "One", "Two", "Three", "Four", "Five", "Six",
			"Seven", "Eight", "Nine", "Ten" };
	private int[] coins = { 5, 10, 25, 100, 200 };
	private int[] costs = { 50, 75, 125, 150, 200, 175, 200, 100, 300, 200 };
	private HardwareSimulator hw;
	private ProductKind[] pk;
	private CoinRackSimulator[] cr;
	private CoinsAvailable ca;
	
	 /**
     * Creates a standard setup as specified by the requirements given, setting up
     * a HardwareSimulator, a ProductKind array, stocking some pop for each rack,
     * and creating a CoinsAvailable object to track the amount of funds available.
     */
	public VendingCore() {
		hw = new HardwareSimulator(coins, costs, names);
		pk = new ProductKind[hw.getNumberOfPopCanRacks()];
		cr = new CoinRackSimulator[hw.getNumberOfCoinRacks()];
		for (int i = 0; i < hw.getNumberOfCoinRacks(); i++) {
			cr[i] = hw.getCoinRack(i);
		}
		// Sets up a coins available object to track the amount of cash
		// available
		ca = new CoinsAvailable(hw.getCoinReceptacle(), coins, cr);
		// Basic outline for setup, pk is needed to see product cost / vend them
		for (int i = 0; i < hw.getNumberOfPopCanRacks(); i++) {
			pk[i] = new ProductKind(names[i], new Currency(
					java.util.Currency.getInstance(Locale.CANADA),
					new BigDecimal(costs[i])), hw.getPopCanRack(i),
					hw.getSelectionButton(i));
		}
	}
	
    /**
     * Inserts a coin of the given value into the vending machine.
     * 
     * @param value
     *            The value of the desired coin to insert.
     */
	public void insertCoin(int value) {

		switch (value) 
		{
		case 5:
			try {
				hw.getCoinSlot().addCoin(new Coin(5));
			} catch (DisabledException e) {
				// TODO Auto-generated catch block
				hw.getDeliveryChute().loadWithoutEvents(new Coin(5));
			}
			break;
		case 10:
			try {
				hw.getCoinSlot().addCoin(new Coin(10));
			} catch (DisabledException e) {
				hw.getDeliveryChute().loadWithoutEvents(new Coin(10));
				
			}
			break;
		case 25:
			try {
				hw.getCoinSlot().addCoin(new Coin(25));
			} catch (DisabledException e) {
				hw.getDeliveryChute().loadWithoutEvents(new Coin(25));
				
			}
			break;
		case 100:
			try {
				hw.getCoinSlot().addCoin(new Coin(100));
			} catch (DisabledException e) {
				hw.getDeliveryChute().loadWithoutEvents(new Coin(100));
				
			}
			break;
		case 200:
			try {
				hw.getCoinSlot().addCoin(new Coin(200));
			} catch (DisabledException e) {
				hw.getDeliveryChute().loadWithoutEvents(new Coin(200));
				
			}
			break;
		default:
			break;
		}

	}
	

	 /**
     * Purchases a pop can from the pop can rack specified by the index.
     * 
     * @param index
     *            The index of pop can rack to dispense a pop from.
     */
	public Object[] buyPop(int index) {
		// basic outline for buying a pop
		// Tests to make sure enough funds are available
		
		if (pk[index].getStandardCost().getQuantity().intValue() <= ca
				.getCoinsAvailable().getQuantity().intValue() && pk[index].getQuantityAvailable() > 0) {
			// Sets up a currency amount to return based on how much the pop can costs
			int retVal = ca.getCoinsAvailable().getQuantity().intValue()
					- pk[index].getStandardCost().getQuantity().intValue();
			Currency ret = new Currency(
					java.util.Currency.getInstance(Locale.CANADA),
					new BigDecimal(retVal));
			// Dispenses the correct pop
			pk[index].dispense();
			// Returns the specified amount of coins, stores the rest.
			ca.returnCoins(ret);
			//System.out.println(ret.getQuantity().intValue());
			ca.storeCoins(pk[index].getStandardCost());
			// Retrieves the items from the delivery chute (pop can and any change)
			Object[] test = hw.getDeliveryChute().removeItems();
			return test;
		} else {
			System.out.println("Insufficient Funds");
			return null;
		}
	}

	 /**
     * Returns unused coins back to the user
     */
	public Object[] returnCoins() {
		// basic outline for coin return
		// on button press:

		// Returns coins from receptacle to delivery chute
		ca.returnCoins(ca.getCoinsAvailable());
		// Removes the coints from the chute then returns them
		Object[] coinsRet = hw.getDeliveryChute().removeItems();
		return coinsRet;
	}
	
    /**
     * Returns a coin rack at the indicated index.
     * 
     * @param index
     *            The index of the desired product.
     * @throws IndexOutOfBoundsException
     *             if the index < 0 or the index >= number of products.
     */
	public ProductKind getProductKind(int index) {
		return pk[index];
	}
	
	public HardwareSimulator getHardwareSimulator() {
		return hw;
	}
	
	public CoinsAvailable getCoinsAvailable() {
		return ca;
	}
	
    /**
     * Returns true if the machine has a minimal combination of coins 
     * necessary to make change between 5 cents and 2 dollars 
     * (1 nickel, 2 dimes, 3 quarters, and 1 loonie)
     */
	
	public boolean checkMinChange() {
		if (hw.getCoinRack(0).getCount() >= 1)
			if (hw.getCoinRack(1).getCount() >= 2)
				if (hw.getCoinRack(2).getCount() >= 3)
					if (hw.getCoinRack(3).getCount() >= 1)
						return true;
		return false;
	}
	

    /**
     * Loads each pop can rack with 3 pops.
     */
	public void loadPop() {
		for (int i = 0; i < 10; i++) 
			for (int j = 0; j < 3; j++) {
				try {
					hw.getPopCanRack(i).addPop(new PopCan());
				} catch (CapacityExceededException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (DisabledException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
	}

}
