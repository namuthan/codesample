package org.lsmr.vendingmachine.core.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.lsmr.vendingmachine.core.VendingCore;
import org.lsmr.vendingmachine.simulator.Coin;

public class CoinReturn {
	private VendingCore core;
	private Object[] ret;
	private Coin coin;
	
	@Before
	public void setUp() throws Exception {
		core = new VendingCore();
	}

	@After
	public void tearDown() throws Exception {
		core = null;
	}

	@Test
	public void returnCoin() {
		// Loads a coin
		core.getHardwareSimulator().getCoinReceptacle().loadWithoutEvents(new Coin(5));
		// Attempts to return the coin
		ret = core.returnCoins();
		coin = (Coin)ret[0];
		assert(coin.getValue() == 5);
	}
	
	@Test
	public void returnMultipleCoins() {
		// Loads multiple coins
		for (int i = 0; i < 5; i++)
			core.getHardwareSimulator().getCoinReceptacle().loadWithoutEvents(new Coin(5));
		// Attempts to return the coins
		ret = core.returnCoins();
		for (int i = 0; i < 5; i++)
		{
			coin = (Coin)ret[i];
			assert(coin.getValue() == 5);
		}
	}
	
	@Test
	public void returnNoFunds() {
		// Attempts to return the coins
		ret = core.returnCoins();
		assert(ret == null);
	}

}
