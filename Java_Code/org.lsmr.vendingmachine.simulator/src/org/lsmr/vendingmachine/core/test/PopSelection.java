package org.lsmr.vendingmachine.core.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.lsmr.vendingmachine.core.VendingCore;
import org.lsmr.vendingmachine.simulator.CapacityExceededException;
import org.lsmr.vendingmachine.simulator.Coin;
import org.lsmr.vendingmachine.simulator.DisabledException;
import org.lsmr.vendingmachine.simulator.EmptyException;
import org.lsmr.vendingmachine.simulator.PopCan;

public class PopSelection {
	private VendingCore core;

	@Before
	public void setUp() throws Exception {
		core = new VendingCore();

	}

	@After
	public void tearDown() throws Exception {
		core = null;
	}

	@Test
	public void insufficientFundsWithStock() throws CapacityExceededException, DisabledException {		
		//Add stock
		core.getHardwareSimulator().getPopCanRack(0).addPop(new PopCan());
	
		assertNull(core.buyPop(0));
		
	}
	
	@Test
	public void insufficientFundsNoStock() throws DisabledException, EmptyException, CapacityExceededException {				
		//Insert funds
		core.getHardwareSimulator().getCoinReceptacle().acceptCoin(new Coin(25));

		assertNull(core.buyPop(0));
		
	}
	
	@Test
	public void sufficientFundsWithStock() throws CapacityExceededException, DisabledException {		
		//Add pop
		core.getHardwareSimulator().getPopCanRack(0).addPop(new PopCan());

		//Insert funds
		core.getHardwareSimulator().getCoinReceptacle().acceptCoin(new Coin(25));
		core.getHardwareSimulator().getCoinReceptacle().acceptCoin(new Coin(25));
		core.getHardwareSimulator().getCoinReceptacle().acceptCoin(new Coin(25));

		assertNotNull(core.buyPop(0));
		
	}
	
	@Test
	public void sufficientFundsNoStock() throws DisabledException, EmptyException, CapacityExceededException {				

		//Insert funds
		core.getHardwareSimulator().getCoinReceptacle().acceptCoin(new Coin(25));
		core.getHardwareSimulator().getCoinReceptacle().acceptCoin(new Coin(25));

		assertNull(core.buyPop(0));
		
	}

	@Test
	public void equalFundsWithStock() throws CapacityExceededException, DisabledException {		
		//Add pop
		core.getHardwareSimulator().getPopCanRack(0).addPop(new PopCan());
		
		//Insert funds
		core.getHardwareSimulator().getCoinReceptacle().acceptCoin(new Coin(25));
		core.getHardwareSimulator().getCoinReceptacle().acceptCoin(new Coin(25));
		
		assertNotNull(core.buyPop(0));
		
	}
	
	@Test
	public void equalFundsNoStock() throws DisabledException, EmptyException, CapacityExceededException {				
		//Insert funds
		core.getHardwareSimulator().getCoinReceptacle().acceptCoin(new Coin(25));
		core.getHardwareSimulator().getCoinReceptacle().acceptCoin(new Coin(25));
		
		assertNull(core.buyPop(0));
		
	}
	
	@Test
	public void noFundsWithStock() {		
		//Add pop
		core.getHardwareSimulator().getPopCanRack(0).loadWithoutEvents(new PopCan());
		
		//Insert funds
		core.insertCoin(0);
		
		assert(core.buyPop(0) == null);
		
	}
	
	@Test
	public void noFundsNoStock() throws DisabledException, EmptyException, CapacityExceededException {				

		assertNull(core.buyPop(0));
		
	}
	

}
