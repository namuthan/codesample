/**
 * 
 */
package org.lsmr.vendingmachine.core.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.lsmr.vendingmachine.core.VendingCore;
import org.lsmr.vendingmachine.simulator.Coin;

/**
 * @author fish
 *
 */
public class CoinInsertion {

	
	VendingCore core;
	Object[] test;
	Coin coin;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		core = new VendingCore();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		core = null;
		coin = null;
		test = null;
	}

	@Test
	public void insertNegativeValue() {
		core.insertCoin(-1);
		assert(core.getCoinsAvailable().getCoinsAvailable().getQuantity().intValue() == 0);
	}
	
	@Test
	public void insertInvalidCoin() {
		core.insertCoin(300);
		assert(core.getCoinsAvailable().getCoinsAvailable().getQuantity().intValue() == 0);
	}
	
	@Test
	public void insertValidCoin() {
		core.insertCoin(25);
		assert(core.getCoinsAvailable().getCoinsAvailable().getQuantity().intValue() == 25);
	}
	
	@Test
	public void insertMultipleValidCoins() {
		core.insertCoin(5);
		core.insertCoin(10);
		core.insertCoin(25);
		core.insertCoin(100);
		core.insertCoin(200);
		assert(core.getCoinsAvailable().getCoinsAvailable().getQuantity().intValue() == 340);
	}
	
	@Test
	public void overflowCoinReceptacle() {
		for (int i = 0; i < 51; i++)
			core.insertCoin(5);
		
		test = core.getHardwareSimulator().getDeliveryChute().removeItems();
		coin = (Coin) test[0];
		assert(coin.getValue() == 5 && test.length == 1);
	}

}
